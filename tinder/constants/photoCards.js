const photoCards = [
  {
    name: 'Samantha',
    age: 24,
    photo: require('../assets/Samantha_At_The_Irumbu_Thirai_Trailer_Launch.jpg'),
    key: 'mFcc5b_t74Q',
  },
  {
    name: 'Angelina Jolie',
    age: 29,
    photo: require('../assets/Angelina_Jolie_Cannes_2007.jpg'),
    key: 'nBywXevf_jE-',
  },
  {
    name: 'Scarlett Johansson',
    age: 22,
    photo: require('../assets/Scarlett_Johansson_by_Gage_Skidmore_2_(cropped).jpg'),
    key: 'caseex6qfO4TPMYyhorner',
  },
  {
    name: 'Alia Bhatt',
    age: 28,
    photo: require('../assets/Alia_Bhatt_promoting_Kalank.jpg'),
    key: 'ozda-XbeP0k',
  },
  {
    name: 'Deepika Padukone',
    age: 30,
    photo: require('../assets/Deepika_Padukone_Cannes_2018_(cropped).jpg'),
    key: 'ZHy0efLnzVc',
  },
  {
    name: 'Elizabeth Olsen',
    age: 21,
    photo: require('../assets/Elizabeth_Olsen_by_Gage_Skidmore_2.jpg'),
    key: 'TvPCUHten1o',
  },
  {
    name: 'Emilia Clarke',
    age: 26,
    photo: require('../assets/Emilia_Clarke_by_Gage_Skidmore_2_(cropped).jpg'),
    key: 'dlbiYGwEe9U',
  },
  {
    name: 'Gal Gadot',
    age: 30,
    photo: require('../assets/Gal_Gadot_2016_lighting_corrected.jpg'),
    key: 'Ml4tr2WO7JE',
  },
  {
    name: 'Trisha',
    age: 28,
    photo: require('../assets/Trisha_at_World_Childrens_Day_Press_Meet.png'),
    key: "Ty4f_NOFO60'",
  },
  {
    name: 'Emma Watson',
    age: 30,
    photo: require('../assets/Emma_Watson_2013.jpg'),
    key: "AvLHH8qYbAI'",
  },
  {
    name: 'Kristen Stewart',
    age: 20,
    photo: require('../assets/Kristen_Stewart_Cannes_2018_(cropped).jpg'),
    key: "3ujVzg9i2EI'",
  },
  {
    name: 'Nayanthara',
    age: 21,
    photo: require('../assets/Nayanthara_at_Filmfare_Awards.jpg'),
    key: "5AoO7dBurMw'",
  },
  {
    name: 'Shriya Saran',
    age: 28,
    photo: require('../assets/Shriya_Saran_at_Bombay_Times_Fashion_Week_(02).jpg'),
    key: 'kYov4x7nTtuOIlGu7HKTQw',
  },
]

export default photoCards